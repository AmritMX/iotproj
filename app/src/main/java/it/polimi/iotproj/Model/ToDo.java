package it.polimi.iotproj.Model;

/**
 * Created by Max on 19/06/18.
 */

public class ToDo {
    private String id, title, description, associatedbeacon, reporter;

    public ToDo() {
    }

    public ToDo(String id, String title, String description, String associatedbeacon, String reporter) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.associatedbeacon = associatedbeacon;
        this.reporter = reporter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssociatedBeacon() {
        return associatedbeacon;
    }

    public void setAssociatedBeacon(String associatedbeacon) {
        this.associatedbeacon = associatedbeacon;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }
}
