package it.polimi.iotproj;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import dmax.dialog.SpotsDialog;
import it.polimi.iotproj.Adapter.LIstItemAdapter;
import it.polimi.iotproj.Model.ToDo;

public class DashboardFragment extends Fragment {

    List<ToDo> toDoList = new ArrayList<>();
    FirebaseFirestore db;

    RecyclerView listItem;
    RecyclerView.LayoutManager layoutManager;

    FloatingActionButton fab;

    public MaterialEditText title, description; //Public so that it is accessible from ListAdapter
    public boolean isUpdate = false; // flag to check if is update or is add new task
    public String idUpdate = ""; //Id of item need to update

    LIstItemAdapter adapter;
    AlertDialog dialog;


    private MainActivity mainActivity;


    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);


        //Init FireStore
        db = FirebaseFirestore.getInstance();


        //View
        dialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .build();

        title = (MaterialEditText)view.findViewById(R.id.title);
        description= (MaterialEditText)view.findViewById(R.id.description);
        fab = (FloatingActionButton)view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add New
                if(!isUpdate)
                {
                    setData(title.getText().toString(), description.getText().toString());
                }
                else
                {
                    updateData(title.getText().toString(), description.getText().toString());
                    isUpdate = !isUpdate; //reset flag
                }
            }
        });

        listItem = (RecyclerView)view.findViewById(R.id.listTodo);
        listItem.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext()); // this or get activity or get context
        listItem.setLayoutManager(layoutManager);

        loadData(); // Load data from FireStore


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity) {
            this.mainActivity = (MainActivity) context;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle().equals("DELETE"))
            deleteItem(item.getOrder());
        return super.onContextItemSelected(item);
    }

    private void deleteItem(int index) {
        switch (this.mainActivity.loggedUserType) {
            case "admin":
                db.collection("ToDoList")
                        .document(toDoList.get(index).getId())
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                loadData();
                            }
                        });
                break;
            case "fixer":
                if(this.mainActivity.nearestBeacon == toDoList.get(index).getAssociatedBeacon()) {
                    db.collection("ToDoList")
                            .document(toDoList.get(index).getId())
                            .delete()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    loadData();
                                }
                            });
                } else {
                    Toast.makeText(getContext(), "Forbidden, Get near the Associated beacon first.", Toast.LENGTH_SHORT).show();
                }

                break;
            case "user":
                db.collection("ToDoList")
                        .document(toDoList.get(index).getId())
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                loadData();
                            }
                        });
                break;
        }
    }

    private void updateData(String title, String description) {
        db.collection("ToDoList").document(idUpdate)
                .update("title",title,"description",description)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(), "Updated !", Toast.LENGTH_SHORT).show();
                    }
                });
        //Realtime update refresh data
        db.collection("ToDoList").document(idUpdate)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        loadData();
                    }
                });
    }

    private void setData(String title, String description) {
        //Random ID
        String id = UUID.randomUUID().toString();
        Map<String,Object> todo = new HashMap<>();
        switch (this.mainActivity.loggedUserType) {
            case "admin":
                todo.put("id",id);
                todo.put("title",title);
                todo.put("description",description);
                todo.put("associatedbeacon", this.mainActivity.nearestBeacon);
                todo.put("reportedby", this.mainActivity.loggedUserID);

                db.collection("ToDoList").document(id)
                        .set(todo).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Refresh data
                        loadData();
                    }
                });
                break;
            case "fixer":
                Toast.makeText(getContext(), "Forbidden, You are not allowed to add Tasks", Toast.LENGTH_SHORT).show();
                break;
            case "user":
                if(this.mainActivity.nearestBeacon != null) {
                    todo.put("id",id);
                    todo.put("title",title);
                    todo.put("description",description);
                    todo.put("associatedbeacon", this.mainActivity.nearestBeacon);
                    todo.put("reportedby", this.mainActivity.loggedUserID);

                    db.collection("ToDoList").document(id)
                            .set(todo).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //Refresh data
                            loadData();
                        }
                    });
                } else {
                    Toast.makeText(getContext(), "Forbidden, Get in Beacon range first.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void loadData() {
        dialog.show();
        if(toDoList.size()>0)
            toDoList.clear(); //Remove old value

        final String userType = this.mainActivity.loggedUserType;
        final String loggedUser = this.mainActivity.loggedUserID;
        final String nearestBeacon = this.mainActivity.nearestBeacon;

        switch (userType) {
            case "admin":
                db.collection("ToDoList")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                for(DocumentSnapshot doc:task.getResult())
                                {
                                    ToDo todo = new ToDo(doc.getString("id"),
                                            doc.getString("title"),
                                            doc.getString("description"),
                                            doc.getString("associatedbeacon"),
                                            doc.getString("reportedby"));
                                    toDoList.add(todo);
                                }
                                adapter = new LIstItemAdapter(DashboardFragment.this , toDoList);
                                listItem.setAdapter(adapter);
                                dialog.dismiss();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case "fixer":
                db.collection("ToDoList")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                for(DocumentSnapshot doc:task.getResult())
                                {
                                    if(doc.getString("associatedbeacon").equals(nearestBeacon)){
                                        ToDo todo = new ToDo(doc.getString("id"),
                                                doc.getString("title"),
                                                doc.getString("description"),
                                                doc.getString("associatedbeacon"),
                                                doc.getString("reportedby"));
                                        toDoList.add(todo);
                                    }
                                }
                                adapter = new LIstItemAdapter(DashboardFragment.this , toDoList);
                                listItem.setAdapter(adapter);
                                dialog.dismiss();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case "user":
                db.collection("ToDoList")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                for(DocumentSnapshot doc:task.getResult())
                                {
                                    if(doc.getString("reportedby").equals(loggedUser)){
                                        ToDo todo = new ToDo(doc.getString("id"),
                                                doc.getString("title"),
                                                doc.getString("description"),
                                                doc.getString("associatedbeacon"),
                                                doc.getString("reportedby"));
                                        toDoList.add(todo);
                                    }
                                }
                                adapter = new LIstItemAdapter(DashboardFragment.this , toDoList);
                                listItem.setAdapter(adapter);
                                dialog.dismiss();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }


}