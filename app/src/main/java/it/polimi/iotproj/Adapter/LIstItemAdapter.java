package it.polimi.iotproj.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import it.polimi.iotproj.DashboardFragment;
import it.polimi.iotproj.MainActivity;
import it.polimi.iotproj.Model.ToDo;
import it.polimi.iotproj.R;
import it.polimi.iotproj.DashboardFragment;

/**
 * Created by Max on 19/06/18.
 */

class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener
{
    ItemClickListener itemClickListener;
    TextView item_title, item_description, item_beaconid, item_reportedby;



    public ListItemViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        itemView.setOnCreateContextMenuListener(this);

        item_title = (TextView)itemView.findViewById(R.id.item_title);
        item_description = (TextView)itemView.findViewById(R.id.item_description);
//        item_beaconid = (TextView)itemView.findViewById(R.id.item_beaconid);
//        item_reportedby = (TextView)itemView.findViewById(R.id.item_reportedby);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Select the action");
        menu.add(0,0,getAdapterPosition(),"DELETE");

    }
}

public class LIstItemAdapter extends RecyclerView.Adapter<ListItemViewHolder> {

    DashboardFragment tasksActivity;
    List<ToDo> todoList;

    public LIstItemAdapter(DashboardFragment tasksActivity, List<ToDo> todoList) {
        this.tasksActivity = tasksActivity;
        this.todoList = todoList;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(tasksActivity.getActivity().getBaseContext());
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {


        //set data for item
        holder.item_title.setText(todoList.get(position).getTitle());
        holder.item_description.setText(todoList.get(position).getDescription());
//        holder.item_beaconid.setText(todoList.get(position).getAssociatedBeacon());
//        holder.item_reportedby.setText(todoList.get(position).getReporter());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                //When user select item, data will be auto set for Edit Text View
                tasksActivity.title.setText(todoList.get(position).getTitle());
                tasksActivity.description.setText(todoList.get(position).getDescription());

                tasksActivity.isUpdate=true; // set flag is update as true
                tasksActivity.idUpdate = todoList.get(position).getId();
            }
        });
    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }
}
