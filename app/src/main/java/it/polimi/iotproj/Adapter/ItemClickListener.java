package it.polimi.iotproj.Adapter;

import android.view.View;

/**
 * Created by Max on 19/06/18.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
