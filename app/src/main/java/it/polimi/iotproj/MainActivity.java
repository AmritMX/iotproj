package it.polimi.iotproj;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.mustard.rx_goodness.rx_requirements_wizard.Requirement;
import com.estimote.mustard.rx_goodness.rx_requirements_wizard.RequirementsWizardFactory;
import com.estimote.proximity_sdk.proximity.EstimoteCloudCredentials;
import com.estimote.proximity_sdk.proximity.ProximityContext;
import com.estimote.proximity_sdk.proximity.ProximityObserver;
import com.estimote.proximity_sdk.proximity.ProximityObserverBuilder;
import com.estimote.proximity_sdk.proximity.ProximityZone;
import com.varunest.sparkbutton.SparkButton;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;

public class MainActivity extends AppCompatActivity {

    private static final int LOGIN_ACTIVITY_REQUEST_CODE = 0;
    private ActionBar toolbar;
    // 0. Add a property to hold the Proximity Observer
    private ProximityObserver proximityObserver;

    // 1. Adding App ID and Token
    private EstimoteCloudCredentials cloudCredentials;



    // Session control variables

    public String loggedUserID = null;
    public String loggedUserType = null;
    public String loggedUserName = null;
    public String nearestBeacon = null;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();

        // Login activity on start
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_ACTIVITY_REQUEST_CODE);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        fragment = HomeFragment.newInstance();
                        toolbar.setTitle("Searching Beacons ...");
                        break;
                    case R.id.navigation_dashboard:
                        fragment = DashboardFragment.newInstance();
                        toolbar.setTitle("All Tasks");
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.commit();
                return true;
            }
        });


        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, HomeFragment.newInstance());
        transaction.commit();


        //Requirements for proximity beacon
        RequirementsWizardFactory
                .createEstimoteRequirementsWizard()
                .fulfillRequirements(this,
                        // onRequirementsFulfilled
                        new Function0<Unit>() {
                            @Override public Unit invoke() {
                                Log.d("app", "requirements fulfilled");
                                proximityObserver.start();
                                return null;
                            }
                        },
                        // onRequirementsMissing
                        new Function1<List<? extends Requirement>, Unit>() {
                            @Override public Unit invoke(List<? extends Requirement> requirements) {
                                Log.e("app", "requirements missing: " + requirements);
                                return null;
                            }
                        },
                        // onError
                        new Function1<Throwable, Unit>() {
                            @Override public Unit invoke(Throwable throwable) {
                                Log.e("app", "requirements error: " + throwable);
                                return null;
                            }
                        });


        //1.a 
        cloudCredentials = new EstimoteCloudCredentials("residence-maintenance-ba7", "c8b41f8cae1fbf4851bc6591ae95e63c");

        // 2. Create the Proximity Observer
        this.proximityObserver =
                new ProximityObserverBuilder(getApplicationContext(), cloudCredentials)
                        .withOnErrorAction(new Function1<Throwable, Unit>() {
                            @Override
                            public Unit invoke(Throwable throwable) {
                                Log.e("MY_TAG", "proximity observer error: " + throwable);
                                Toast.makeText(getApplicationContext(), "proximity observer error", Toast.LENGTH_SHORT).show();
                                return null;
                            }
                        })
                        .withBalancedPowerMode()
                        .build();

        toolbar.setTitle("Searching Beacons ...");

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);

    }

    // This method is called when the Login activity finishes successfully
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == LOGIN_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // get String data from Intent
                loggedUserType = data.getStringExtra("keyLoggedUserType");
                loggedUserID = data.getStringExtra("keyLoggedUserID");
                loggedUserName = data.getStringExtra("keyLoggedUserName");


                // set text view with string
                TextView textView = (TextView) findViewById(R.id.loggedinuser);
                textView.setText(loggedUserName);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                beaconMan();
                Toast.makeText(getApplicationContext(), "Searching", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void beaconMan() {

        // Zone:
        ProximityZone zone = this.proximityObserver.zoneBuilder()
                .forTag("iotproj")
                .inNearRange()
                .withOnEnterAction(new Function1<ProximityContext, Unit>() {
                    @Override
                    public Unit invoke(ProximityContext context) {
                        String block = context.getAttachments().get("block");
                        Log.d("app", "Welcome to " + block + "-Block");






                        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.fragment_container);
                        String msg = "Welcome to " + block + "-Block";
                        homeFragment.setStatus1(msg);





                        return null;
                    }
                })
                .withOnChangeAction(new Function1<List<? extends ProximityContext>, Unit>() {
                    @Override
                    public Unit invoke(List<? extends ProximityContext> contexts) {
                        List<String> rooms = new ArrayList<>();
                        List<String> ids = new ArrayList<>();
                        for (ProximityContext context : contexts) {
                            rooms.add(context.getAttachments().get("room"));
                            ids.add(context.getInfo().getDeviceId());
                        }
                        Log.d("app", "In range of room(s): " + rooms);



                        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.fragment_container);
                        String msg = "In range of room(s): " + rooms.toString();

                        nearestBeacon = ids.toString();

                        homeFragment.setStatus2(msg);
                        homeFragment.animatespark();




                        return null;
                    }
                })
                .withOnExitAction(new Function1<ProximityContext, Unit>() {
                    @Override
                    public Unit invoke(ProximityContext context) {
                        Log.d("app", "Bye bye, come again!");
                        return null;
                    }
                })
                .create();
        this.proximityObserver.addProximityZone(zone);


    }



}
