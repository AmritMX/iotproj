package it.polimi.iotproj;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.varunest.sparkbutton.SparkButton;

public class HomeFragment extends Fragment {

    private MainActivity mainActivity;
    private TextView statusText1, statusText2;
    private SparkButton sparkButton;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        statusText1 = (TextView) view.findViewById(R.id.status1TextView);
        statusText2 = (TextView) view.findViewById(R.id.status2TextView);
        sparkButton = (SparkButton) view.findViewById(R.id.spark_button);



        this.mainActivity.beaconMan();

        (view.findViewById(R.id.spark_button)).setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity) {
            this.mainActivity = (MainActivity) context;
        }
    }

    public void setStatus1(String status) {
        statusText1.setText(status);
    }

    public void setStatus2(String status) {
        statusText2.setText(status);
    }

    public void animatespark() {
        sparkButton.playAnimation();
    }
}